package com.gitlab.seventhcircle.sevenfarm;


/*
 * This class is responsible for watching for the block interaction that causes soil to be turned into
 * dirt and act on it accordingly to the config settings which this class also checks each time the command is issued
 * to ensure always up to date info!
 */


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

class CropTrampleEvent implements Listener {

    //Event designed to handle the soil trample protection
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onTrample(PlayerInteractEvent event) {

        SevenProtectCommand protectcommand = new SevenProtectCommand();


        //config check for global setting
        if (!(protectcommand.getGlobalProtect())) {
            if (event.getAction() == Action.PHYSICAL) {

                Block block = event.getClickedBlock();
                Player player = event.getPlayer();

                if (block == null) {
                    return;
                }

                Material blockType = block.getType();

                //Checks to ensure that the event in question is in fact soil being trampled
                if (blockType == Material.SOIL) {
                    event.setUseInteractedBlock(Event.Result.DENY);
                    event.setCancelled(true);
                    block.setType(blockType);
                    player.sendMessage("[GLOBAL] Your Event was canceled");
                }
            }

            //config check for local setting
        } else if (!(protectcommand.getLocalProtect(event.getPlayer()))) {
            if (event.getAction() == Action.PHYSICAL) {

                Block block = event.getClickedBlock();
                Player player = event.getPlayer();

                if (block == null) {
                    return;
                }

                Material blockType = block.getType();

                //Checks to ensure that the event in question is in fact soil being trampled
                if (blockType == Material.SOIL) {
                    event.setUseInteractedBlock(Event.Result.DENY);
                    event.setCancelled(true);
                    block.setType(blockType);
                    player.sendMessage("[LOCAL] Your Event was canceled");
                }
            }
        }
    }
}

package com.gitlab.seventhcircle.sevenfarm;

/*
 * SevenCommand is the class which executes the only technically command 7f from there args are passed to their relevant
 * areas to ensure a cleaner environment.
*/

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

class SevenCommand implements CommandExecutor {

    // The Main CommandSender Method
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        // Receives commands and send em where they need to go
        if (commandLabel.equalsIgnoreCase("7f")) {

            if (!(args.length >= 1)) {
                errorTwo(sender, commandLabel);
                return false;
            }
            if (args[0].equalsIgnoreCase("set")) {
                setController(sender, args);
                return true;
            }
            if (args[0].equalsIgnoreCase("get")) {
                getController(sender, args);
                return true;
            }
            if (args[0].equalsIgnoreCase("help")) {
                helpMessage(sender);
                return true;
            } else {
                errorOne(sender, args[0]);
                return false;
            }
        }
        return false;
    }


    private void setController(CommandSender sender, String[] args) {

        if (args[1].equalsIgnoreCase("global")) {
            if (!(sender.hasPermission("7farm.setglobal"))) {
                lacksPermissionMessage(sender, args[1]);
                return;
            }

            SevenProtectCommand gprotectcommand = new SevenProtectCommand();
            gprotectcommand.setGlobalProtect(args[2], sender);
            return;
        }

        if (args[1].equalsIgnoreCase("local")) {
            if (requiresPlayer(sender)) {
                if (!(sender.hasPermission("7farm.setlocal"))) {
                    lacksPermissionMessage(sender, args[1]);
                    return;
                }

                SevenProtectCommand lprotectcommand = new SevenProtectCommand();
                Player player = (Player) sender;
                lprotectcommand.setLocalProtect(args[2], player);
            }
        } else {
            errorTwo(sender, args[1]);
        }
    }


    private void getController(CommandSender sender, String[] args) {

        if (args[1].equalsIgnoreCase("global")) {
            if (!(sender.hasPermission("7farm.getglobal"))) {
                lacksPermissionMessage(sender, args[1]);
                return;
            }

            SevenProtectCommand gprotectcommand = new SevenProtectCommand();
            Boolean value = gprotectcommand.getGlobalProtect();
            sender.sendMessage(ChatColor.GRAY + "Global Crop Trample is currently set to: " + ChatColor.AQUA + value.toString());
            return;
        }

        if (args[1].equalsIgnoreCase("local")) {
            if (requiresPlayer(sender)) {
                if (!(sender.hasPermission("7farm.getlocal"))) {
                    lacksPermissionMessage(sender, args[1]);
                    return;
                }

                SevenProtectCommand lprotectcommand = new SevenProtectCommand();
                Player player = (Player) sender;
                Boolean value = lprotectcommand.getLocalProtect(player);
                sender.sendMessage(ChatColor.GRAY + "Your Crop Trample is currently set to: " + ChatColor.AQUA + value.toString());
            }
        } else {
            errorTwo(sender, args[1]);
        }
    }


    private void helpMessage(CommandSender sender) {
        sender.sendMessage(ChatColor.AQUA
                + " 7Farm Commands\n "
                + ChatColor.GRAY + "+-+-+-+-+-+-+-+-+-+-+ \n"
                + ChatColor.AQUA + " Get <local/global> \n "
                + "Set <local/global> <true/false>\n "
                + "Help");
    }


    private void lacksPermissionMessage(CommandSender sender, String commandLabel) {
        sender.sendMessage(ChatColor.DARK_RED + "You lack permission to issue the command: " + ChatColor.GRAY + commandLabel);
    }


    private void errorOne(CommandSender sender, String commandLabel) {
        sender.sendMessage(ChatColor.DARK_RED + "Error! " + ChatColor.GRAY + commandLabel + ChatColor.DARK_RED + " Is not a Command.");
        helpMessage(sender);
    }


    private void errorTwo(CommandSender sender, String commandLabel) {
        sender.sendMessage(ChatColor.DARK_RED + "Error! " + ChatColor.GRAY + commandLabel + ChatColor.DARK_RED + " Improper arguments for that command");
        helpMessage(sender);
    }


    private void errorThree(CommandSender sender) {
        sender.sendMessage(ChatColor.DARK_RED + "Error! Unexpected behaviour has occurred!");
        helpMessage(sender);
    }


    private boolean requiresPlayer(CommandSender sender) {
        boolean isplayer = false;
        if (sender instanceof Player) {
            isplayer = true;
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "Error! This command requires you to be a player!");
        }
        return isplayer;
    }
}

package com.gitlab.seventhcircle.sevenfarm;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class OnJoinEvent implements Listener {

    //Event for ensuring players get added to the config settings for soil trample
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onJoin(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (!(player.hasPlayedBefore())) {
            SevenConfig config = new SevenConfig();

            config.addPlayer(player.getUniqueId().toString());
        }
    }
}

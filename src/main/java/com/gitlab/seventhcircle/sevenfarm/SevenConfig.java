package com.gitlab.seventhcircle.sevenfarm;

class SevenConfig {

    //Responsible for storing all the player settings and the single global variable
    private SevenFarm plugin = SevenFarm.getPlugin(SevenFarm.class);

    void configloading() {
        plugin.getConfig().addDefault("GlobalSoilTrample", true);
        plugin.getConfig().options().copyDefaults(true);
    }

    void addPlayer(String uuid) {
        plugin.getConfig().addDefault(uuid, true);
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
    }

    void setPlayer(String uuid, Boolean value) {
        if (!plugin.getConfig().isSet(uuid)) {
            addPlayer(uuid);
        }

        plugin.getConfig().set(uuid, value);
    }

    Boolean getPlayer(String uuid) {
        return plugin.getConfig().getBoolean(uuid);
    }

    Boolean getGlobal() {
        return plugin.getConfig().getBoolean("GlobalSoilTrample");
    }

    void setGlobal(Boolean value) {
        plugin.getConfig().set("GlobalSoilTrample", value);
    }

    void saveConfigs() {
        plugin.saveConfig();
    }
}

package com.gitlab.seventhcircle.sevenfarm;

/*
 * This class handles everything to do with crop harvesting and handing out the drops depending on
 * several variables!
 */

import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;

class CropHarvest implements org.bukkit.event.Listener {

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onHarvest(BlockBreakEvent event)
    {
        // Ints used for determining drops
        int drop = 0;
        int enchbonus = 0;
        int lootamount = 0;

        Block block = event.getBlock();
        ItemStack item = event.getPlayer().getInventory().getItemInMainHand();

        // Determines Itemtype and assigns values accordingly

        drop = toolDrop(item);

        // Checks if the tool is enchanted with fortune

        enchbonus = enchBonus(item);

        // Crop type Checker and drop distributor!
        lootamount = enchbonus + drop;

        lootSelector(block, lootamount, item);

    }

    private void giveRandomLoot(Block block) {
        RandomLoot randomLoot = new RandomLoot();
        Material randomdrop = randomLoot.getRandomLoot();
        block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(randomdrop, 1));
    }

    private int toolDrop(ItemStack item) {
        Material itemtype = item.getType();
        int drop = 0;

        switch (itemtype) {
            case WOOD_HOE:
            case STONE_HOE:
                drop = 1;
                removeDurability(item);
                break;

            case IRON_HOE:
            case GOLD_HOE:
                drop = 2;
                removeDurability(item);
                break;

            case DIAMOND_HOE:
                drop = 3;
                removeDurability(item);
                break;

            default:
                break;
        }

        return drop;
    }

    private void randomLootCheck(ItemStack item, Block block) {
        Material itemtype = item.getType();
        if (itemtype.equals(Material.DIAMOND_HOE)) {
            giveRandomLoot(block);
        }
    }

    private void removeDurability(ItemStack item) {
        int enchantmentlevel = item.getEnchantmentLevel(Enchantment.DURABILITY);
        short durability = item.getDurability();

        if (item.getDurability() >= item.getType().getMaxDurability()) {
            item.setAmount(0);
        }

        switch (enchantmentlevel) {
            case 1:
                durability += 3;
                item.setDurability(durability);
                break;
            case 2:
                durability += 2;
                item.setDurability(durability);
                break;
            case 3:
                durability += 1;
                item.setDurability(durability);
                break;
            default:
                durability += 4;
                item.setDurability(durability);
                break;

        }

    }

    private int enchBonus(ItemStack item) {
        int enchantmentlevel = item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        int enchbonus = 0;

        switch (enchantmentlevel) {
            case 1:
                enchbonus = 1;
                break;
            case 2:
                enchbonus = 2;
                break;
            case 3:
                enchbonus = 3;
                break;
            default:
                break;
        }
        return enchbonus;
    }

    private void lootSelector(Block block, int lootamount, ItemStack item) {
        Material blocktype = block.getType();
        MaterialData blockdata = block.getState().getData();
        switch (blocktype) {
            case CROPS:
                block.setType(Material.AIR);
                if (((Crops) blockdata).getState() != CropState.RIPE) break;
                dropItemSeeded(block, Material.WHEAT, Material.SEEDS, lootamount, item);
                break;

            case POTATO:
                setToAir(block);
                if (((Crops) blockdata).getState() != CropState.RIPE) break;
                dropItemUnSeeded(block, Material.POTATO_ITEM, lootamount, item);
                break;

            case CARROT:
                setToAir(block);
                if (((Crops) blockdata).getState() != CropState.RIPE) break;
                dropItemUnSeeded(block, Material.CARROT_ITEM, lootamount, item);


            case COCOA:
                setToAir(block);
                block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.INK_SACK, lootamount, (short) 3));
                break;

            case SUGAR_CANE_BLOCK:
                setToAir(block);
                if (hasBeenPlaced(block)) break;
                dropItemUnSeeded(block, Material.SUGAR_CANE, lootamount, item);
                break;

            case CACTUS:
                setToAir(block);
                if (hasBeenPlaced(block)) break;
                dropItemUnSeeded(block, Material.CACTUS, lootamount, item);
                break;

            case MELON_BLOCK:
                setToAir(block);
                if (hasBeenPlaced(block)) break;
                dropItemSeeded(block, Material.MELON, Material.MELON_SEEDS, lootamount, item);
                break;

            case PUMPKIN:
                setToAir(block);
                if (hasBeenPlaced(block)) break;
                dropItemSeeded(block, Material.PUMPKIN, Material.PUMPKIN_SEEDS, lootamount, item);
                break;

            case BEETROOT_BLOCK:
                setToAir(block);
                if (((Crops) blockdata).getState() != CropState.RIPE) break;
                dropItemSeeded(block, Material.BEETROOT, Material.BEETROOT_SEEDS, lootamount, item);
                break;

            case NETHER_STALK:
                setToAir(block);
                if (((Crops) blockdata).getState() != CropState.RIPE) break;
                dropItemUnSeeded(block, Material.NETHER_WARTS, lootamount, item);
                break;

            default:
                break;
        }
    }

    private void dropItemSeeded(Block block, Material materialone, Material materialtwo, int lootamount, ItemStack item) {
        randomLootCheck(item, block);
        block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(materialone, lootamount));
        block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(materialtwo, lootamount));
    }

    private void dropItemUnSeeded(Block block, Material materialone, int lootamount, ItemStack item) {
        randomLootCheck(item, block);
        block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(materialone, lootamount));
    }

    private void setToAir(Block block) {
        block.setType(Material.AIR);
    }

    private boolean hasBeenPlaced(Block block) {
        return block.hasMetadata("playerplaced");
    }
}

package com.gitlab.seventhcircle.sevenfarm;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

class SevenProtectCommand {
    private SevenConfig sevenconfig = new SevenConfig();
    //The Class responsible for the behaviour of the set of protect commands

    void setGlobalProtect(String value, CommandSender sender) {
        Boolean bvalue = Boolean.valueOf(value);
        sevenconfig.setGlobal(bvalue);

        sender.sendMessage(ChatColor.GRAY + "Global Setting Trample Setting is set to: " + ChatColor.AQUA + getGlobalProtect().toString());
    }

    Boolean getGlobalProtect() {
        return sevenconfig.getGlobal();
    }

    void setLocalProtect(String value, Player player) {
        Boolean bvalue = Boolean.valueOf(value);
        String uuid = player.getUniqueId().toString();

        sevenconfig.setPlayer(uuid, bvalue);
        player.sendMessage(ChatColor.GRAY + "Your Trample Setting now equals: " + ChatColor.AQUA + getLocalProtect(player).toString());

    }

    Boolean getLocalProtect(Player player) {
        return sevenconfig.getPlayer(player.getUniqueId().toString());
    }
}


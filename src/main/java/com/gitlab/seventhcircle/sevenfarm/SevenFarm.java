package com.gitlab.seventhcircle.sevenfarm;

/*
Main Class where only necessary functions are occurring and nothing more.
 */

import org.bukkit.plugin.java.JavaPlugin;


public final class SevenFarm extends JavaPlugin {
    private SevenConfig config;

    @Override
    // Start up Logic
    public void onEnable(){

        config = new SevenConfig();
        config.configloading();

        registerCommands();
        registerEvents();
    }

    @Override
    public void onDisable() {
        config.saveConfigs();
    }

    private void registerCommands() {
        getCommand("7f").setExecutor(new SevenCommand());
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new CropTrampleEvent(), this);
        getServer().getPluginManager().registerEvents(new CropHarvest(), this);
        getServer().getPluginManager().registerEvents(new OnJoinEvent(), this);
        getServer().getPluginManager().registerEvents(new OnBlockPlaceEvent(), this);
    }
}

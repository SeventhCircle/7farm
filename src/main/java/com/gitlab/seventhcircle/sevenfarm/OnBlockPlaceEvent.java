package com.gitlab.seventhcircle.sevenfarm;


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class OnBlockPlaceEvent implements Listener {

    private SevenFarm plugin = SevenFarm.getPlugin(SevenFarm.class);

    //Used to stop the vast majority of duping of specific crops
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        Material blocktype = block.getType();
        switch (blocktype) {
            case CACTUS:
            case PUMPKIN:
            case MELON_BLOCK:
            case SUGAR_CANE_BLOCK:
                block.setMetadata("playerplaced", new FixedMetadataValue(plugin, true));
                break;

            default:
                break;
        }
    }

}

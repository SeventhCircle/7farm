package com.gitlab.seventhcircle.sevenfarm;

import org.bukkit.Material;

import java.util.Random;

//This class handles all the random loot gen for diamond hoes

class RandomLoot {
    private int getRandomNumber (){
        Random randomlootnumber = new Random();
        return randomlootnumber.nextInt(10) + 1;
    }

    Material getRandomLoot() {
        RandomLoot randomnumber = new RandomLoot();
        int lootnumber = randomnumber.getRandomNumber();

        switch (lootnumber) {
            case 1:
                return Material.ANVIL;
            case 2:
                return Material.APPLE;
            case 3:
                return Material.ARROW;
            case 4:
                return Material.STONE_HOE;
            case 5:
                return Material.DARK_OAK_DOOR_ITEM;
            case 6:
                return Material.DIAMOND_LEGGINGS;
            case 7:
                return Material.GOLD_BLOCK;
            case 8:
                return Material.BIRCH_DOOR;
            case 9:
                return Material.DIRT;
            case 10:
                return Material.STONE;
            default:
                return Material.AIR;
        }
    }
}
